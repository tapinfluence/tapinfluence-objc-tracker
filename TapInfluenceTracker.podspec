Pod::Spec.new do |s|
  s.name             = "TapInfluenceTracker"
  s.version          = "0.7.0.5"
  s.summary          = "TapInfluence event tracker for iOS 7+. Add analytics to your iOS apps and games."
  s.description      = <<-DESC
  TapInfluence Tracker is a mobile and event analytics platform with a difference: rather than tell our users how they should analyze their data, we deliver their event-level data in their own data warehouse, on their own Amazon Redshift or Postgres database, so they can analyze it any way they choose. TapInfluence Tracker mobile is used by data-savvy games companies and app developers to better understand their users and how they engage with their games and applications. TapInfluence Tracker is open source using the business-friendly Apache License, Version 2.0 and scales horizontally to many billions of events.
                       DESC
  s.homepage         = "https://www.tapinfluence.com/"
  s.screenshots      = "https://d3i6fms1cm1j0i.cloudfront.net/github-wiki/images/snowplow-logo-large.png"
  s.license          = 'Apache License, Version 2.0'
  s.author           = { "TapInfluence" => "mobilesdks@tapinfluence.com" }
  s.source           = { :git => "https://bitbucket.org/tapinfluence/tapinfluence-objc-tracker.git", :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/TapInfluence'
  s.documentation_url	= 'https://bitbucket.org/tapinfluence/tapinfluence-objc-tracker'

  s.ios.deployment_target = '8.0'

  s.requires_arc = true

  s.preserve_paths = 'TapInfluenceTracker.framework'
  s.vendored_frameworks = 'TapInfluenceTracker.framework'

  s.ios.frameworks = 'CoreTelephony', 'UIKit', 'Foundation'
  s.dependency 'FMDB', '2.6.2'
  s.ios.dependency 'Reachability', '3.2'
end
